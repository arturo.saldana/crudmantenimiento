/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import modelo.domain.Persona;
import modelo.dto.PersonaDto;
import modelo.dto.PersonaListDto;
import persistencia.PersonaDao;

/**
 *
 * @author cartu
 */
public class PersonLogic {
    
    private final PersonaDao personaDao = new PersonaDao();
    
    public List<PersonaListDto> list() {
        List<Persona> listPersona = personaDao.list();
        List<PersonaListDto> listaPersonasDto = new ArrayList<>();
        
        for (Persona persona : listPersona) {
            PersonaListDto pDto = new PersonaListDto();
            pDto.setId(persona.getId());
            pDto.setDni(persona.getDni());
            pDto.setNombre(persona.getNombre());
            pDto.setApellidos(persona.getApellidos());
            listaPersonasDto.add(pDto);
        }
        return listaPersonasDto;
    }
    
    public boolean create(PersonaDto personaDto) {
        Persona persona = new Persona();
        persona.setDni(personaDto.getDni());
        persona.setNombre(personaDto.getNombre());
        persona.setApellidos(personaDto.getApellidos());
        persona.setFechaRegistro(Date.from(Instant.now()));
        
        int creado = personaDao.create(persona);
        return creado == 1;
    }
    public PersonaListDto read(Integer id) {
        Persona persona = personaDao.read(id);
        PersonaListDto pDto = new PersonaListDto();
        pDto.setId(persona.getId());
        pDto.setDni(persona.getDni());
        pDto.setNombre(persona.getNombre());
        pDto.setApellidos(persona.getApellidos());
        
        return pDto;
    }
    
    public boolean update(Integer id, PersonaDto personaDto) {
        Persona persona = personaDao.read(id);
        persona.setDni(personaDto.getDni());
        persona.setNombre(personaDto.getNombre());
        persona.setApellidos(personaDto.getApellidos());
        
        int actualizado = personaDao.update(persona);
        return actualizado == 1;
    }
    
    public boolean delete(Integer id) {
        int eliminado = personaDao.delete(id);
        
        return eliminado==1;
    }
}
