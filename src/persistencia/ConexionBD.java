/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author cartu
 */
public class ConexionBD {
    
    //Variables accesibles desde cualquier clase del proyecto
    public static Connection cn = null;
    public static Statement st = null;
    public static PreparedStatement ps = null;
    public static ResultSet rs = null;
    
    //Variables accesibles sólo desde esta clase
    private static String servidor = "localhost";
    private static String nombreBD = "bd_persona";
    private static String usuario = "root";
    private static String clave = "";

    public static boolean conectar() {
        if (cn == null) {
            try {

                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://" + servidor + ":3306/" + nombreBD;
                cn = DriverManager.getConnection(url, usuario, clave);

            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Conexión no realizada con el servidor MySQL", "Error de conexion", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null, "Fue imposible conectarse al servidor.", "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Se ha encontrado el siguiente error" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }
        if (cn != null) {
            return true;
        }
        return false;
    }

    public static void cerrarConexion() {
        if (cn != null) {
            try {
                if (!cn.isClosed()) {
                    cn.close();
                }
            } catch (SQLException es) {
            }
        }
    }

    public static Connection getConnection() {
        return cn;
    }

}
