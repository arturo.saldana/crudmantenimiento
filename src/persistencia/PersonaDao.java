/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.domain.Persona;
import static persistencia.ConexionBD.*;

/**
 *
 * @author cartu
 */
public class PersonaDao {
    
    public List<Persona> list() {
        String sql = "SELECT id, dni, nombre, apellidos FROM persona";
        List<Persona> listaPersonas = new ArrayList<>();
        conectar();
        
        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Persona persona = new Persona();
                persona.setId(rs.getInt(1));
                persona.setDni(rs.getString(2));
                persona.setNombre(rs.getString(3));
                persona.setApellidos(rs.getString(4));
                
                listaPersonas.add(persona);
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        //cerrarConexion();
        return listaPersonas;
    }
    
    public int create(Persona persona) {
        int rsu = 0;
        String sql = "INSERT INTO persona(dni, nombre, apellidos, fecha_registro) VALUES (?,?,?,?)";
            conectar();
        try {
            ps = cn.prepareStatement(sql);
            ps.setString(1, persona.getDni());
            ps.setString(2, persona.getNombre());
            ps.setString(3, persona.getApellidos());
            ps.setTimestamp(4, new java.sql.Timestamp(persona.getFechaRegistro().getTime()));
            rsu = ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        
        //cerrarConexion();
        return rsu;
    }
    
    public Persona read(Integer id) {
        String sql = "SELECT id, dni, nombre, apellidos FROM persona WHERE id = " + id;
        Persona persona = null;
            conectar();
        try {
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                persona = new Persona();
                persona.setId(rs.getInt(1));
                persona.setDni(rs.getString(2));
                persona.setNombre(rs.getString(3));
                persona.setApellidos(rs.getString(4));
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        //cerrarConexion();
        return persona;
    }
    
    public int update(Persona persona) {
        int rsu = 0;
        String sql = "UPDATE persona SET dni = ?, nombre = ?, apellidos = ? WHERE id = ? ";
        conectar();
        try {
            ps = cn.prepareStatement(sql);
            ps.setString(1, persona.getDni());
            ps.setString(2, persona.getNombre());
            ps.setString(3, persona.getApellidos());
            ps.setInt(4, persona.getId());
            rsu = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        //cerrarConexion();
        return rsu;
    }
    
    public int delete(Integer id) {
        int rsu = 0;
        String sql = "DELETE FROM persona WHERE id = ? ";
        conectar();
        
        try {
            ps = cn.prepareStatement(sql);
            ps.setInt(1, id);
            rsu = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        //cerrarConexion();
        return rsu;
    }
    
}
